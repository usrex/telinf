import 'package:flutter/material.dart';
import 'package:teleferico/Screems/CompraBoleto.dart';
import 'package:teleferico/Screems/DebitCard.dart';

import 'package:teleferico/Screems/LsTele.dart';

class TelefericoList extends StatefulWidget {
  @override
  _TelefericoListState createState() => _TelefericoListState();
}

class _TelefericoListState extends State<TelefericoList> {
  int _indexCurrent = 0;
  final List<Widget> _children = [
    LsTel(),
    CompraBoleto(),
    DebitCard(),
    //DisplaysWidget(color: Colors.red,),
  ];

  Color cl = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _children[_indexCurrent],
      bottomNavigationBar: _buttonNav(),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _indexCurrent = index;
    });
  }

  Widget _buttonNav() {
    return BottomNavigationBar(
      backgroundColor: Colors.black,
      currentIndex: _indexCurrent,
      onTap: onTabTapped,
      fixedColor: Colors.blue,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(
            Icons.traffic,
            color: _indexCurrent == 0 ? Colors.blue : Colors.grey,
          ),
          title: Text(
            'Lineas',
            style: TextStyle(
                color: _indexCurrent == 0 ? Colors.blue : Colors.grey),
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.add_shopping_cart,
            color: _indexCurrent == 1 ? Colors.blue : Colors.grey,
          ),
          title: Text('Comprar Boleto',
              style: TextStyle(
                  color: _indexCurrent == 1 ? Colors.blue : Colors.grey)),
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.credit_card,
              color: _indexCurrent == 2 ? Colors.blue : Colors.grey,
            ),
            title: Text('Agregar Pago',
                style: TextStyle(
                    color: _indexCurrent == 2 ? Colors.blue : Colors.grey)))
      ],
    );
  }

  Widget _appBar() {
    return AppBar(
      centerTitle: true,
      title: Text('Usuario?'),
      elevation: 100,
      backgroundColor: Colors.black,
      toolbarOpacity: 0.6,
    );
  }
}
