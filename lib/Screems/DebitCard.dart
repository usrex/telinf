import 'package:flutter/material.dart';

class DebitCard extends StatefulWidget {
  @override
  _DebitCardState createState() => _DebitCardState();
}

class _DebitCardState extends State<DebitCard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _formCArd(),
    );
  }

  Widget _formCArd() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(left: 15, right: 15, top: 15),
        child: Column(
          children: <Widget>[
            Text(
              "Aregar Tarjeta",
              style: TextStyle(fontSize: 25, color: Colors.blue),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.credit_card),
                  labelText: "Numero Tarjeta",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.2)))),
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: 15,
                ),
                Container(
                  width: 150,
                  child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.date_range),
                        labelText: "YY",
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.2)))),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                  width: 150,
                  child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.email),
                        labelText: "MM",
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.2)))),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              keyboardType: TextInputType.datetime,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.vpn_key),
                  labelText: "Code",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.2)))),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.phone),
                  labelText: "Phone",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.2)))),
            ),
            SizedBox(
              height: 15,
            ),
            RaisedButton(
              onPressed: () {},
              child: Text(
                "Agregar Tarjeta",
                style: TextStyle(color: Colors.white),
              ),
              shape: StadiumBorder(),
              splashColor: Colors.white,
              color: Colors.black,
            )
          ],
        ),
      ),
    );
  }
}
