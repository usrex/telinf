import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class LsTel extends StatefulWidget {
  @override
  _LsTelState createState() => _LsTelState();
}

class _LsTelState extends State<LsTel> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _listTeleferic(context),
    );
  }

  Widget _listTeleferic(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(left: 15, right: 15, top: 10),
      children: <Widget>[
        _cardList(context, "Teleferico Rojo", 3, "assets/rojo.png"),
        _cardList(context, "Teleferico Azul", 4, "assets/azul.jpeg"),
        _cardList(context, "Teleferico Verde", 2, "assets/verde.png"),
        _cardList(context, "Teleferico Celeste", 1, "assets/celeste.jpeg"),
        _cardList(context, "Teleferico Morado", 5, "assets/morado.png"),
      ],
    );
  }

  void show(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            content: Container(
              height: 250,
              width: 250,
              child: Image(
                image: AssetImage(
                  "assets/pp.jpg",
                ),
                fit: BoxFit.cover,
              ),
            ),
          );
        });
  }

  Widget _cardList(
      BuildContext context, String nameTel, int starts, String url) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      elevation: 5,
      child: Container(
        margin: EdgeInsets.only(top: 5),
        child: InkWell(
          onTap: () => show(context),

          //{
          //showAboutDialog(context: context);
          //},

          splashColor: Colors.black87,
          child: Column(
            children: <Widget>[
              Container(
                padding:
                    EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      nameTel,
                      style:
                          TextStyle(fontStyle: FontStyle.italic, fontSize: 20),
                    ),
                    RatingBar(
                      itemSize: 20,
                      initialRating: 0,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                        size: 10,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    )
                    /*               Row(
                      children: _star(starts),
                    ),*/
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, bottom: 10),
                child: Row(
                  children: <Widget>[
                    ClipOval(
                      child: Image(
                        image: AssetImage(url),
                        height: 100,
                        width: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text("Usuarios"),
                              Text("123456")
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Text("Paradas"),
                              Text("Estacion central / cementerio")
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _star(int number) {
    List<Widget> stars = [];
    int i = 0;
    for (i = 0; i < 5; i++) {
      if (int.parse(i.toString()) < number) {
        Icon i = Icon(
          Icons.star,
          color: Colors.yellow,
        );
        stars.add(i);
      } else {
        Icon i = Icon(Icons.star);
        stars.add(i);
      }
    }
    return stars;
  }
}
